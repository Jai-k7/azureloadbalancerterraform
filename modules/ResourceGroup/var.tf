variable "RGname" {
    description = "Resource Group name"
}

variable "RGlocation" {
    description = "Resource Group location"
}