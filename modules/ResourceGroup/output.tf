output "name" {
    value = azurerm_resource_group.newRG.name
}
output "location" {
    value = azurerm_resource_group.newRG.location
}