resource "azurerm_virtual_network" "network" {
  name                    = var.app_name
  address_space           = var.AddressSpace
  location                = var.vnetlocation
  resource_group_name     = var.vnetrg

}
resource "azurerm_subnet" "subnet" {
  count                   = length(var.subnet_names)
  name                    = var.subnet_names[count.index]
  resource_group_name     = var.vnetrg
  virtual_network_name    = azurerm_virtual_network.network.name
  address_prefix          = var.subnet_prefixes[count.index]

}

