variable "app_name" {
    description = "Virtual network name"
}

variable "AddressSpace" {
    description = "Address Range of Virtual network"
}

variable "vnetlocation" {
    description = "Vnet Location"
}

variable "vnetrg" {
    description = "Vnet Resource group name"
}

variable "subnet_prefixes" {
  description = "The address prefix to use for the subnet."
  default     = ["10.0.1.0/24"]
}

variable "subnet_names" {
  description = "A list of public subnets inside the vNet."
  default     = ["subnet1", "subnet2", "subnet3"]
}