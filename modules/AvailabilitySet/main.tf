resource "azurerm_availability_set" "vm" {
  name                         = var.avsetName
  location                     = var.location
  resource_group_name          = var.rgname
  platform_fault_domain_count  = 2
  platform_update_domain_count = 2
  managed                      = true
}