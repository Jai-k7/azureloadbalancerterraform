variable "avsetName" {
    description = "Availibility Set Name"
}

variable "location" {
    description = "Resource Group location"
}

variable "rgname" {
    description = "Resource Group Name"
}