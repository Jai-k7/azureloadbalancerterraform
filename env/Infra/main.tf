provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
    version         = "=1.44.0"
}

#################################
#      RESOURCE GROUP
#################################
module "resource-group" {
    source          = "../../modules/ResourceGroup"
    RGname          = var.ResourceGroupName
    RGlocation      = lookup(var.ResourceGroupLocation,terraform.workspace)
}

#################################
#      VIRTUAL NETWORK
#################################
module "Vnet1" {
    source          = "../../modules/Vnet"
    app_name        = var.Vnet1Name
    AddressSpace    = var.Vnet1AddressSpace
    vnetlocation    = module.resource-group.location
    vnetrg          = module.resource-group.name
    subnet_prefixes = var.Subnet_Prefix 
    subnet_names    =  var.Subnet_names
}

#################################
#      Availability Set
#################################
module "Availibity_Set" {
    source          = "../../modules/AvailabilitySet"
    avsetName       = var.AVsetName
    location        = module.resource-group.location
    rgname          = module.resource-group.name

    
}

#################################
#      Terraform Backend
#################################

terraform {
  backend "azurerm" {
    resource_group_name  = "TerraformBackend"
    storage_account_name = "storestatefile"
    container_name       = "default-container"
    key                  = "terraform.tfstate"
  }
}
