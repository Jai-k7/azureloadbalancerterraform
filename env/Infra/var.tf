#########################################
#          RESOURCE GROUP VARIABLES
#########################################
variable "ResourceGroupName" {
    default = "DEV-TEST"
}

variable "ResourceGroupLocation" {
    type = map(string)
    default = {
      default = "West Europe"
      dev = "East US"
      prd = "Central Canada"
    }
}

#########################################
#          VIRTUAL NETWORK VARIABLES
#########################################
variable "Vnet1Name"{
    default = "TEST-VNET-1"
}

variable "Vnet1AddressSpace" {
    default = ["10.0.0.0/16"]
}

variable "Subnet_Prefix" {
  description = "The address prefix to use for the subnet."
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]

}

variable "Subnet_names" {
  description = "A list of public subnets inside the vNet."
  default     = ["subnet1", "subnet2", "subnet3"]
}

#########################################
#          Availability Set Variable
#########################################
variable "AVsetName" {
  default = "lb-avset"
}

